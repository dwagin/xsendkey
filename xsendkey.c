/*
 *
 *   Author: Dmitry Wagin <dmitry.wagin@ya.ru>
 *
 *   This program is distributed under the GPL license.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xlib.h>

void usage()
{
	fprintf(stderr,
	        "usage: xsendkey [-d display] [-h] [-v] Keys\n"
	        " -d    display:     Display. ie: 127.0.0.1:0.0\n"
	        " -v                 Verbose\n"
	        " -h                 Help. This screen.\n"
	        " Keys               Keys to send\n\n\n");

	exit(1);
}

int main(int argc, char **argv)
{
	int ch;
	int verbose = 0;
	char *display_name = NULL;
	Display *display;
	char *s, *sl;
	unsigned int modifier = 0;
	KeyCode keycode = 0;
	XEvent event;


	/* Parse Command Line Arguments */
	while ((ch = getopt(argc, argv, "vd:h")) != -1) {
		switch (ch) {
		case 'v':
			verbose = 1;
			break;
		case 'd':
			if ((display_name = strdup(optarg)) == NULL) {
				abort();
			}
			break;
		case 'h':
			usage();
			break;
		default:
			usage();
			break;
		}
	}

	argc -= optind;
	argv += optind;

	if (argc <= 0) {
		fprintf(stderr, "Error: Too few arguments\n");
		usage();
	}

	if ((s = strdup(argv[0])) == NULL) {
		abort();
	}

	if (display_name == NULL) {
		display_name = XDisplayName(NULL);
	}

	if ((display = XOpenDisplay(display_name)) == NULL) {
		fprintf(stderr, "Error: XOpenDisplay for %s\n", display_name);
		exit(1);
	}

	while ((sl = strsep(&s, "+ ")) != NULL) {
		if (*sl != '\0') {
			if (strcasecmp(sl, "control") == 0 || strcasecmp(sl, "ctrl") == 0) {
				modifier |= ControlMask;
			} else if (strcasecmp(sl, "shift") == 0) {
				modifier |= ShiftMask;
			} else if (strcasecmp(sl, "mod1") == 0 || strcasecmp(sl, "alt") == 0) {
				modifier |= Mod1Mask;
			} else if (strcasecmp(sl, "mod2") == 0) {
				modifier |= Mod2Mask;
			} else if (strcasecmp(sl, "mod3") == 0) {
				modifier |= Mod3Mask;
			} else if (strcasecmp(sl, "mod4") == 0) {
				modifier |= Mod4Mask;
			} else if (strcasecmp(sl, "mod5") == 0) {
				modifier |= Mod5Mask;
			} else {
				keycode = XKeysymToKeycode(display, XStringToKeysym(sl));
			}
		}
	}

	if (verbose) {
		printf("Display  is: %s\n", display_name);
		printf("Modifier is: 0x%x\n", modifier);
		printf("Keycode  is: 0x%x\n", keycode);
	}

	event.xkey.type = KeyPress;
	event.xkey.window = RootWindow(display, DefaultScreen(display));
	event.xkey.root = event.xkey.window;
	event.xkey.subwindow = 0x0;
	event.xkey.state = modifier;
	event.xkey.keycode = keycode;
	event.xkey.same_screen = True;
	event.xkey.display = display;
	event.xkey.send_event = False;

	XSendEvent(display, event.xkey.root, True, KeyPressMask, &event);

	XCloseDisplay(display);

	exit(0);
}
